import subprocess
import os

def test_ledger_is_installed():
    subprocess.run('ledger --version', shell=True, check=True)

def test_ledger_finds_init_file():
    subprocess.run('ledger accounts', shell=True, check=True)

def test_git_is_installed():
    subprocess.run('git --version', shell=True, check=True)

def test_man_is_installed():
    subprocess.run('man --version', shell=True, check=True)
