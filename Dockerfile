FROM ubuntu:bionic

RUN apt-get update && apt-get install -y \
    ledger \
    git \
    man

RUN mkdir -- /ledger

WORKDIR /ledger
